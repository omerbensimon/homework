import requests
from bs4 import BeautifulSoup
import pytest


def test_alibaba(page_text):
    text_to_find = 'Alibaba'.lower()
    assert page_text.lower().find(text_to_find) >= 0, 'The string "{}" was not found in' \
                                                      ' the page\'s html'.format(text_to_find)


def test_right_click_msg(page_text):
    text_to_find = "Right-click in the box below to see one called 'the-internet'".lower()
    assert page_text.lower().find(text_to_find) >= 0, 'The string "{}" was not found in' \
                                                      ' the page\'s html'.format(text_to_find)


@pytest.fixture(scope="session")
def page_text():
    res = requests.get('https://the-internet.herokuapp.com/context_menu')
    html_page = res.content
    soup = BeautifulSoup(html_page, 'html.parser')
    text = soup.find_all(text=True)

    output = ''
    blacklist = [
        '[document]',
        'noscript',
        'header',
        'html',
        'meta',
        'head',
        'input',
        'script',
        # there may be more elements you don't want, such as "style", etc.
    ]

    for t in text:
        if t.parent.name not in blacklist:
            output += '{} '.format(t)

    return return output
    